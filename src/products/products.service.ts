import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private ProductRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.ProductRepository.save(createProductDto);
  }

  findAll() {
    return this.ProductRepository.find();
  }

  findOne(id: number) {
    return this.ProductRepository.findOne({ where: { id } });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.ProductRepository.findOneBy({ id });
    if (!product) {
      throw new NotFoundException();
    }
    const updatedProduct = { ...product, ...updateProductDto };
    return this.ProductRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.ProductRepository.findOneBy({ id });
    if (!product) {
      throw new NotFoundException();
    }
    return this.ProductRepository.softRemove(product);
  }
}
